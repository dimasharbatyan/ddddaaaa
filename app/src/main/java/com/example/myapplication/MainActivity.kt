package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast


class MainActivity : AppCompatActivity() {

    private lateinit var nameTextView: TextView
    private lateinit var nextButton: Button
    private lateinit var surnameTextView: TextView
    private lateinit var mailTextView: TextView
    private lateinit var passwordTextView: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        nameTextView = findViewById(R.id.et_user_name)
        nextButton = findViewById(R.id.buttonPanel)
        surnameTextView = findViewById(R.id.et_user_surname)
        mailTextView = findViewById(R.id.et_mail)
        passwordTextView = findViewById(R.id.et_password)

        nextButton.setOnClickListener() {

            if(nameTextView.length()<2){
                Toast.makeText(this, "სახელი უნდა შეიცავდეს 1-ზე მეტ სიმბოლოს.",Toast.LENGTH_SHORT).show()
            }

            if(surnameTextView.length()<5){
                Toast.makeText(this, "გვარი უნდა შეიცავდეს 5-ზე მეტ სიმბოლოს.", Toast.LENGTH_SHORT).show()
            }

            else Toast.makeText(this, "გილოცავთ, თქვენ წარმატებით გაიარეთ რეგისტრაცია.", Toast.LENGTH_SHORT).show()
        }

        if (nameTextView.text.trim().isNotEmpty() && mailTextView.length()
                .equals(10) && mailTextView.text.trim().isNotEmpty()){

        }



    }
}